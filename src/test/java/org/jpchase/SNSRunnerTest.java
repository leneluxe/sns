package org.jpchase;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions

(strict = false, features = { "classpath:features" }, glue = { "org.jpchase.sns.stepdefs" }, format = {}, tags = {
		"test" }, plugin = { "pretty", "html:target/html", "json:target/cucumber.json" })
// use this class to trigger all the tests

public class SNSRunnerTest {

}


