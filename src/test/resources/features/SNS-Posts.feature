Feature:
  As a user of the social network service
  I  want to be able to share content to the social netowrk
  So other users can view and critique my posts

  Background:
    #Given the user deletes all posts and related comments against userid: "1" 
     
  @smoke
  Scenario: Verify successful post submission against user account (happy path)
    Given the user accesses the POSTS service
    When the user creates a post with the following details and submits:
     |id			|1							|
     |userid	|1							|
     |title		|test						|
     |body		|gibberrish post|
    Then the user should get a "201" response
   
    When the user interrogates the POSTS endpoint
    Then the user should see the recently submitted post
    
  Scenario: Verify user not able to create post against non-existent user account (unhappy path)
    Given the user accesses the POSTS service
    When the user creates a post with the following details and submits:
     |id			|1							|
     |userid	|11							|
     |title		|test						|
     |body		|gibberrish post|
    Then the user should get a "400" response
   
    When the user interrogates the POSTS endpoint
    Then the user should not see the recently submitted post
   
  Scenario: Verify successful post submission against user account (happy path - max values all fields)
    Given the user accesses the POSTS service
    When the user creates a post with the following details and submits:
     |id			|1 							|
     |userid	|1							|
     |title		|testskdfdfdfd	|
     |body		|gibberrish post|
    Then the user should get a "201" response
   
    When the user interrogates the POSTS endpoint
    Then the user should see the recently submitted post
    
  Scenario: Verify successful post submission against user account (happy path - minimum values all fields)
    Given the user accesses the POSTS service
    When the user creates a post with the following details and submits:
     |id			|1 							|
     |userid	|1							|
     |title		|t							|
     |body		|g							|
    Then the user should get a "201" response
   
    When the user interrogates the POSTS endpoint
    Then the user should see the recently submitted post
    
  Scenario: Verify successful post submission against user account (unhappy path - mandatory fields)
    Given the user accesses the POSTS service
    When the user creates a post with the following details and submits:
     |id			|1 							|
     |userid	|1							|
     |title		| 							|
     |body		|								|
    Then the user should get a "400" response
   
    When the user interrogates the POSTS endpoint
    Then the user should not see the recently submitted post