Feature:
  As a user of the social network service
  I  want to be able to contribute comments to posts on the social netowrk
  So other users can view and critique them

  Background:
    #Given the user deletes all posts and related comments against userid: "1" 
    
  @smoke
  Scenario: Verify successful submission of comment against post for user account (happy path)
    Given the user accesses the POSTS service
    And the user creates a post with the following details and submits:
     |id			|1							|
     |userid	|1							|
     |title		|test						|
     |body		|gibberrish post|
    Then the user should get a "201" response
    
    Given the user accesses the COMMENTS service
    When the user submits a comment against a post with the following details:
      |postid	|1								|
      |id			|1								|
      |name		|test							|
      |email	|test@mail.com		|
      |body		|gibberish comment|
    Then the user should get a "201" response
  
    When the user interrogates the COMMENTS endpoint
    Then the user should see the recently submitted comment

  Scenario: Verify user not able to post comment against non-existent post (unhappy path)
    Given the user accesses the COMMENTS service
    When the user submits a comment against a post with the following details:
      |postid	|1								|
      |id			|1								|
      |name		|test							|
      |email	|test@mail.com		|
      |body		|gibberish comment|
    Then the user should get a "201" response
  
    When the user interrogates the COMMENTS endpoint
    Then the user should not see the recently submitted comment
    
  Scenario: Verify successful submission of comment against post for user account (happy path - maximum values all fields)
    Given the user accesses the POSTS service
    And the user creates a post with the following details and submits:
     |id			|1							|
     |userid	|1							|
     |title		|test						|
     |body		|gibberrish post|
    Then the user should get a "201" response
    
    Given the user accesses the COMMENTS service
    When the user submits a comment against a post with the following details:
      |postid	|1								|
      |id			|1								|
      |name		|test	xxxxxxxxxxx	|
      |email	|test@mail.comxxx	|
      |body		|gibberish comment|
    Then the user should get a "201" response
  
    When the user interrogates the COMMENTS endpoint
    Then the user should see the recently submitted comment
    
  Scenario: Verify successful submission of comment against post for user account (happy path - minimum values all fields)
    Given the user accesses the POSTS service
    And the user creates a post with the following details and submits:
     |id			|1							|
     |userid	|1							|
     |title		|test						|
     |body		|gibberrish post|
    Then the user should get a "201" response
    
    Given the user accesses the COMMENTS service
    When the user submits a comment against a post with the following details:
      |postid	|1								|
      |id			|1								|
      |name		|t								|
      |email	|t@mail.com				|
      |body		|g								|
    Then the user should get a "201" response
  
    When the user interrogates the COMMENTS endpoint
    Then the user should see the recently submitted comment
   
    
  Scenario: Verify successful submission of comment against post for user account (unhappy path - invalid fields)
    Given the user accesses the POSTS service
    And the user creates a post with the following details and submits:
     |id			|1							|
     |userid	|1							|
     |title		|test						|
     |body		|gibberrish post|
    Then the user should get a "201" response
    
    Given the user accesses the COMMENTS service
    When the user submits a comment against a post with the following details:
      |postid	|1								|
      |id			|1								|
      |name		|test							|
      |email	|test.mail.comxxx	|
      |body		|gibberish comment|
    Then the user should get a "400" response
  
    When the user interrogates the COMMENTS endpoint
    Then the user should not see the recently submitted comment
    
  Scenario: Verify successful submission of comment against post for user account (unhappy path - mandatory fields)
    Given the user accesses the POSTS service
    And the user creates a post with the following details and submits:
     |id			|1							|
     |userid	|1							|
     |title		|test						|
     |body		|gibberrish post|
    Then the user should get a "201" response
    
    Given the user accesses the COMMENTS service
    When the user submits a comment against a post with the following details:
      |postid	|1								|
      |id			|1								|
      |name		|									|
      |email	|test.mail.comxxx	|
      |body		|									|
    Then the user should get a "400" response
  
    When the user interrogates the COMMENTS endpoint
    Then the user should not see the recently submitted comment
    
  Scenario: Verify successful submission of comment against post for user account (unhappy path - invalid characters all fields)
    Given the user accesses the POSTS service
    And the user creates a post with the following details and submits:
     |id			|1							|
     |userid	|1							|
     |title		|test						|
     |body		|gibberrish post|
    Then the user should get a "201" response
    
    Given the user accesses the COMMENTS service
    When the user submits a comment against a post with the following details:
      |postid	|1								|
      |id			|1								|
      |name		|&^&&*						|
      |email	|?$&*�@.mail.comx	|
      |body		|!"�$�						|
    Then the user should get a "400" response
  
    When the user interrogates the COMMENTS endpoint
    Then the user should not see the recently submitted comment
