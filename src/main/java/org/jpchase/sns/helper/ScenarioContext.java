package org.jpchase.sns.helper;

/**
 * Use this class to store whatever variables / references you need to use between different tests / scenarios. Note, variable must use a static reference
 */
public class ScenarioContext {
	
	public static String lastTitleValue;
	public static String lastBodyValue;
	public static String lastPostId;
	public static String lastName;
	public static String lastEmail;

}
