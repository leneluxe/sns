package org.jpchase.sns.helper;

import java.io.IOException;
import java.io.InputStream;
import java.util.NoSuchElementException;
import java.util.Properties;

/**
 * Class to load property files for SUT on a per environment basis - if no
 * configuration passed, default environment used will be DEFAULT_ENVIRONMENT
 * variable
 */
public class PropertiesUtil {

	private static Properties configProperties = new Properties();
	private static String configFile;

	private final static String DEFAULT_ENVIRONMENT = "sns";
	static {
		configFile = (System.getProperty("env") != null ? System.getProperty("env") : DEFAULT_ENVIRONMENT)
				+ "-config.properties";

		System.out.println("Loading environment config file:" + configFile);
		ClassLoader loader = Thread.currentThread().getContextClassLoader();

		try (InputStream resourceStream = loader.getResourceAsStream(configFile)) {
			configProperties.load(resourceStream);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException nul) {
			throw new ExceptionInInitializerError(configFile + " not found. Please create this configuration file");
		}
	}

	public static String getConfigPropertyValue(String key) {
		return configProperties.getProperty(key);

	}
}