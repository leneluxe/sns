package org.jpchase.sns.utility;

/**
 * Use this class to initialize payloads using string templates for various journies
 */
public class PayloadInitializer {
	
	public String getPayloadWithReplacedValuesForPosts(String userid, String id, String title, String body) {
		String payload = "{\r\n" + 
				"        \"userId\": " + userid + ",\r\n" + 
				"        \"id\": " + id + ",\r\n" + 
				"        \"title\": \" " + title + "\",\r\n" + 
				"        \"body\": \" " + body + "\"\r\n" + 
				"    }";
		return payload;
	}
	
	public String getPayloadWithReplacedValuesForComments(String postid, String id, String name, String email, String body) {
	
		String payload = "{\r\n" + 
			"        \"postId\": " + postid + ",\r\n" + 
			"        \"id\": " + id + ",\r\n" + 
			"        \"name\": \" " + name + "\",\r\n" + 
			"        \"email\": \" " + email + "\",\r\n" + 
			"        \"body\": \" " + body + "\"\r\n" + 
			"    }";
		return payload;
	}

}
