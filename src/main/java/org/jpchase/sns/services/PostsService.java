package org.jpchase.sns.services;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import org.jpchase.sns.enums.Endpoints;
import org.jpchase.sns.helper.PropertiesUtil;
import org.jpchase.sns.stepdefs.SNSStepDefs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PostsService {

	public Response response;
	public RequestSpecification rs;

	private static final Logger LOG = LoggerFactory.getLogger(PostsService.class);

	public RequestSpecification getRS() {
		// ignore any security certificate prompts
		RestAssured.useRelaxedHTTPSValidation();

		// build the first part of the request i.e. required headers, base url
		rs = given().contentType("application/json").baseUri(PropertiesUtil.getConfigPropertyValue("sns_base_url"));
		return rs;
	}

	public Response makePost(String payload) {
		// complete building request (include payload) and issue POST action to api

		response = rs.body(payload).log().all().when().post(Endpoints.POSTS.getEndpoint());
		LOG.info("Response payload: " + response.asString());
		return response;
	}

	public Response makeGetRequest() {
		// make get request to posts api endpoint
		response = rs.log().all().when().get(Endpoints.POSTS.getEndpoint());
		LOG.info("Response payload: " + response.asString());
		return response;
	}
}
