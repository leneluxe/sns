package org.jpchase.sns.services;

import static io.restassured.RestAssured.given;

import org.jpchase.sns.enums.Endpoints;
import org.jpchase.sns.helper.PropertiesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class CommentsService {
	
	protected Response response;
	protected RequestSpecification rs;

	private static final Logger LOG = LoggerFactory.getLogger(CommentsService.class);

	public RequestSpecification getRS() {
		// ignore any security certificate prompts
		RestAssured.useRelaxedHTTPSValidation();

		// build the first part of the request i.e. required headers, base url & port number
		rs = given().contentType("application/json").baseUri(PropertiesUtil.getConfigPropertyValue("sns_base_url"));
		return rs;
	}

	public Response makePost(String payload) {
		// complete building request (include payload) and issue POST action to api
		response = rs.body(payload).log().all().when().post(Endpoints.COMMENTS.getEndpoint());
		LOG.info("... Response payload: " + response.asString());
		return response;
	}

	public Response makeGetRequest() {
		// make get request to comments api endpoint
		response = rs.log().all().when().get(Endpoints.COMMENTS.getEndpoint());
		LOG.info("... Response payload: " + response.asString());
		return response;
	}

}
