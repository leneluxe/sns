package org.jpchase.sns.stepdefs;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.jpchase.sns.helper.ScenarioContext;
import org.jpchase.sns.services.CommentsService;
import org.jpchase.sns.services.PostsService;
import org.jpchase.sns.utility.PayloadInitializer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;

public class SNSStepDefs {

	private PostsService postsService;
	private CommentsService commentsService;
	private PayloadInitializer payloadInitializer;
	private Response response;

	public SNSStepDefs() {
		postsService = new PostsService();
		commentsService = new CommentsService();
		payloadInitializer = new PayloadInitializer();
	}

	private static final Logger LOG = LoggerFactory.getLogger(SNSStepDefs.class);

	@Given("^the user accesses the POSTS service$")
	public void the_user_accesses_the_POSTS_service() throws Throwable {
		LOG.info("... Build request to make post ...");
		postsService.getRS();
	}

	@Given("^the user creates a post with the following details and submits:$")
	public void the_user_creates_a_post_with_the_following_details(Map<String, String> data) throws Throwable {
		LOG.info("... Build post request ...");
		String payload = "";
		ScenarioContext.lastPostId = data.get("id");
		ScenarioContext.lastTitleValue = data.get("title");
		ScenarioContext.lastBodyValue = data.get("body");
		payload = payloadInitializer.getPayloadWithReplacedValuesForPosts(data.get("id"), data.get("userid"),
				data.get("title"), data.get("body"));
		response = postsService.makePost(payload);
	}

	@Then("^the user should get a \"([^\"]*)\" response$")
	public void the_user_should_receive_a_response(String status) throws Throwable {
		LOG.info("... Rest Call returned with status code: " + response.getStatusCode());
		Assert.assertEquals(status, String.valueOf(response.getStatusCode()));
	}

	@When("^the user interrogates the POSTS endpoint$")
	public void the_user_interrogates_the_POSTS_endpoint() throws Throwable {
		LOG.info("... Make get request to posts endpoint...");
		response = postsService.makeGetRequest();
	}

	@Then("^the user should see the recently submitted post$")
	public void the_user_should_see_the_recently_submitted_post() throws Throwable {
		LOG.info("... Check previously submitted title & body are present in response payoad ...");
		Assert.assertTrue(checkKeyValuePairPresentInJsonResponse("title", ScenarioContext.lastTitleValue));
		Assert.assertTrue(checkKeyValuePairPresentInJsonResponse("body", ScenarioContext.lastTitleValue));
	}

	@Given("^the user deletes all posts and related comments against userid: \"([^\"]*)\"$")
	public void the_all_posts_and_related_comments_against_userid_are_deleted(String arg1) throws Throwable {
		// call fixture/api call (to be provided) to delete posts/comments against specified userid for
		// housekeeping purposes so tests can be self-contained
	}

	@When("^the user submits a comment against a post with the following details:$")
	public void the_user_submits_a_comment_against_a_post_with_the_following_details(Map<String, String> data)
			throws Throwable {
		LOG.info("... Build comments request and then submit...");
		String payload = "";
		ScenarioContext.lastName = data.get("name");
		ScenarioContext.lastEmail = data.get("email");
		ScenarioContext.lastBodyValue = data.get("body");
		payload = payloadInitializer.getPayloadWithReplacedValuesForComments(data.get("postid"), data.get("id"),
				data.get("name"), data.get("email"), data.get("body"));
		response = commentsService.makePost(payload);
	}

	@Given("^the user accesses the COMMENTS service$")
	public void the_user_accesses_the_COMMENTS_service() throws Throwable {
		LOG.info("... Build request to post comment ...");
		commentsService.getRS();
	}

	@When("^the user interrogates the COMMENTS endpoint$")
	public void the_user_interrogates_the_COMMENTS_endpoint() throws Throwable {
		LOG.info("... Make get request to posts endpoint ...");
		response = commentsService.makeGetRequest();
	}

	@Then("^the user should see the recently submitted comment$")
	public void the_user_should_see_the_recently_submitted_comment() throws Throwable {
		LOG.info("... Check previously submitted name, email & body are present in response payoad ...");
		Assert.assertTrue(checkKeyValuePairPresentInJsonResponse("name", ScenarioContext.lastName));
		Assert.assertTrue(checkKeyValuePairPresentInJsonResponse("email", ScenarioContext.lastEmail));
		Assert.assertTrue(checkKeyValuePairPresentInJsonResponse("body", ScenarioContext.lastBodyValue));
	}
	
	@Then("^the user should not see the recently submitted post$")
	public void the_user_should_not_see_the_recently_submitted_post() throws Throwable {
		LOG.info("... Check previously submitted title & body are not present in response payoad ...");
		Assert.assertFalse(checkKeyValuePairPresentInJsonResponse("title", ScenarioContext.lastTitleValue));
		Assert.assertFalse(checkKeyValuePairPresentInJsonResponse("body", ScenarioContext.lastTitleValue));
	}

	@Then("^the user should not see the recently submitted comment$")
	public void the_user_should_not_see_the_recently_submitted_comment() throws Throwable {
		LOG.info("... Check previously submitted name, email & body are not present in response payoad ...");
		Assert.assertFalse(checkKeyValuePairPresentInJsonResponse("name", ScenarioContext.lastName));
		Assert.assertFalse(checkKeyValuePairPresentInJsonResponse("email", ScenarioContext.lastEmail));
		Assert.assertFalse(checkKeyValuePairPresentInJsonResponse("body", ScenarioContext.lastBodyValue));
	}
	
	public boolean checkKeyValuePairPresentInJsonResponse(String key, String value) {
		JSONArray jsonArray = new JSONArray(response.body().asString());
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			if (jsonObject.has(key) && jsonObject.get(key).equals(value)) {
				return true;
			}
		}
		return false;
	}
}
