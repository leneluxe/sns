package org.jpchase.sns.enums;

import org.jpchase.sns.helper.ScenarioContext;

/**
 * Use this class to capture api paths for services
 */
public enum Endpoints {

	//Social network service endpoints, capture all new endpoint paths here
	POSTS("/posts"),
	COMMENTS("/comments"),
	USERS("/users");
	
	private String endpoint; 
	
	Endpoints(String endpointurl){
		this.endpoint = endpointurl;
	}
	
	public String getEndpoint() {
		return endpoint;
	}
	
}

